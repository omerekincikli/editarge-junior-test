package konsolProje;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

public class Xml2Json {
	
	public static void main(String[] args) throws IOException, JSONException {
		String xmlFile = System.getProperty("user.dir") + "\\dosya2.xml";
		 
		String xmlString = new String(Files.readAllBytes(Paths.get(xmlFile)));
		JSONObject xmlJSONObj = XML.toJSONObject(xmlString);
		 
		String jsonFile = System.getProperty("user.dir") + "\\dosya2.json";
		 
		try (FileWriter fileWriter = new FileWriter(jsonFile)){
			fileWriter.write(xmlJSONObj.toString(4));
		}
	}
}
