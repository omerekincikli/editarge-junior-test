package konsolProje;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

public class Json2Xml {
	
	public static void main(String[] args) throws IOException, JSONException {

		String jsonStr;
		String jsonFile = System.getProperty("user.dir") + "\\dosya1.json";
		String xmlFile = System.getProperty("user.dir") + "\\dosya1.xml";

		jsonStr = new String(Files.readAllBytes(Paths.get(jsonFile)));
		JSONObject json = new JSONObject(jsonStr);

		try (FileWriter fileWriter = new FileWriter(xmlFile)){
			fileWriter.write(XML.toString(json));
		}
	}
}