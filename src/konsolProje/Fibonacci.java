package konsolProje;

import java.math.BigInteger;
import java.util.Scanner;

public class Fibonacci {

	public static void main(String args[])
	{
		Scanner scan = new Scanner(System.in);
		BigInteger baslangicDeger = BigInteger.valueOf(0);
		BigInteger ikinciDeger = BigInteger.valueOf(1);
		BigInteger ucuncuDeger = BigInteger.valueOf(1);
		System.out.print("Ba�lang�� de�erini giriniz:");

		try{
			baslangicDeger=scan.nextBigInteger();
			scan.close();

			if(baslangicDeger.intValue()!=0) ikinciDeger = baslangicDeger;

			System.out.print(baslangicDeger+" "+ikinciDeger);
			for(int i=2;i<600;i++){
				ucuncuDeger =  baslangicDeger.add(ikinciDeger);
				System.out.print(" "+ucuncuDeger);
				baslangicDeger = ikinciDeger;
				ikinciDeger = ucuncuDeger;
			}
			System.out.println("\n600. de�er:"+ucuncuDeger);
		}
		catch(Exception e){
			System.out.println("Girilen de�er hatal�");
		}
	}
}
